package com.example.pagos.vo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import android.util.Log;

public class UsuarioVO  {

	long id_local, id_server;
	String nombre;
	int cedula;
	String apellidos, correo, password;
	long saldo;
	String cookie;
	String id_n_push;

	public UsuarioVO() {
		super();
	}

	public UsuarioVO(long id_local, long id_server, String nombre, int cedula,
			String apellidos, String correo, String password, long saldo,String cookie,String id_n_push) {
		super();
		this.id_local = id_local;
		this.id_server = id_server;
		this.nombre = nombre;
		this.cedula = cedula;
		this.apellidos = apellidos;
		this.correo = correo;
		this.password = password;
		this.saldo = saldo;
		this.cookie = cookie;
		this.id_n_push =id_n_push;
	}

	public long getId_local() {
		return id_local;
	}

	public void setId_local(long id_local) {
		this.id_local = id_local;
	}

	public long getId_server() {
		return id_server;
	}

	public void setId_server(long id_server) {
		this.id_server = id_server;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCedula() {
		return cedula;
	}

	public void setCedula(int cedula) {
		this.cedula = cedula;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getSaldo() {
		return saldo;
	}

	public void setSaldo(long saldo) {
		this.saldo = saldo;
	}

	public String getCookie() {
		return cookie;
	}

	public void setCookie(String cookie) {
		this.cookie = cookie;
	}

	public String getId_n_push() {
		return id_n_push;
	}

	public void setId_n_push(String id_n_push) {
		this.id_n_push = id_n_push;
	}

	

}

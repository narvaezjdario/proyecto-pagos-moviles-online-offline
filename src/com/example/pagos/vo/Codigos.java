package com.example.pagos.vo;

public class Codigos {	
	
	public static final int OPERACION_REGISTRO=0;
	public static final int OPERACION_LOGIN=1;
	public static final int OPERACION_PAGAR=2;
	public static final int OPERACION_TRANSFERIR=3;
	public static final int OPERACION_CERRAR_SESION=4;

}

package com.example.pagos.data.controller;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.pagos.data.db.SQLiteDatabaseAdapter;
import com.example.pagos.vo.UsuarioVO;

public class PagosDBController {

	private static PagosDBController instancia;

	private SQLiteDatabaseAdapter aSQLiteDatabaseAdapter = null;
	private static SQLiteDatabase db;
	private Cursor dbCursor;
	private Context contexto;

	public static final String TABLE_USUARIO = "usuario";

	public static final String CU_ID_LOCAL = "id_local";
	public static final String CU_ID_SERVER = "id_server";
	public static final String CU_NOMBRE = "nombre";
	public static final String CU_CEDULA = "cedula";
	public static final String CU_APELLIDOS = "apellidos";
	public static final String CU_CORREO = "correo";
	public static final String CU_PASSWORD = "password";
	public static final String CU_SALDO = "saldo";
	public static final String CU_COOKIE= "cookie";
	public static final String CU_ID_N_PUSH= "id_n_push";

	public PagosDBController(Context contexto) {
		this.contexto = contexto;
		if (aSQLiteDatabaseAdapter == null) {
			aSQLiteDatabaseAdapter = SQLiteDatabaseAdapter
					.getInstance(contexto);
			db = aSQLiteDatabaseAdapter.getWritableDatabase();
		}
	}

	public static PagosDBController getInstancia(Context contexto) {
		if (instancia == null)
			return instancia = new PagosDBController(contexto);
		return instancia;
	}

	/**
	 * Definimos lista de columnas de la tabla para utilizarla en las consultas
	 * a la base de datos
	 */

	private String[] columnasusuario = new String[] { 
			CU_ID_LOCAL , CU_ID_SERVER, CU_NOMBRE, 
			CU_CEDULA   , CU_APELLIDOS, CU_CORREO,
			CU_PASSWORD , CU_SALDO    , CU_COOKIE,
			CU_ID_N_PUSH

	};
	
	////////////OPERACIONES PARA LA TABLA USUARIO////////////////////
	
	
	/**
	 * Devuelve cursor con todos las columnas de la tabla
	 */
	public Cursor getCursorUsuario() throws SQLException {
		Cursor c = db.query(true, TABLE_USUARIO, columnasusuario, null, null, null, null, null, null);

		return c;
	}

	/**
	 * Devuelve lista de todos los usuarios con todos las columnas de la tabla
	 */
	public List<UsuarioVO> getCursorListUsuario() throws SQLException {
		Cursor c = db.query(true, TABLE_USUARIO, columnasusuario, null, null, null, null, null, null);

		List<UsuarioVO> listUsuario= new ArrayList<UsuarioVO>();

		while (c.moveToNext()) {

			UsuarioVO usuario = new UsuarioVO();

			usuario.setId_local((long)c.getInt(0));
			usuario.setId_server((long)c.getInt(1));
			usuario.setNombre(c.getString(2));
			usuario.setCedula(c.getInt(3));

			usuario.setApellidos(c.getString(4));
			usuario.setCorreo(c.getString(5));
			usuario.setPassword(c.getString(6));
			usuario.setSaldo((long)c.getInt(7));
			
			usuario.setCookie(c.getString(8));
			usuario.setId_n_push(c.getString(9));

			listUsuario.add(usuario);
		}
		return listUsuario;
	}

	/**
	 * Devuelve cursor con todos las columnas del registro
	 */
	public Cursor getRegistroUsuario(long id) throws SQLException {
		String sql = "SELECT * from " + TABLE_USUARIO + " WHERE " + CU_ID_LOCAL + "=?";
		String args[] = new String[]{String.valueOf(id)};
		Cursor c = db.rawQuery(sql, args);

		// Nos movemos al primer registro de la consulta
			c.moveToFirst();
		return c;
	}

	/**
	 * Inserta los valores en un registro de la tabla
	 */
	public boolean insertUsuario(ContentValues reg) {
		boolean verificacion = false;

		// if (db == null)
		// abrir();
		try {
			Log.i("insert", "antes de insertar");
			long id = db.insertWithOnConflict(TABLE_USUARIO, null, reg, SQLiteDatabase.CONFLICT_REPLACE);
			Log.i("Registro", "id de Usuario: " + id);
			Log.i("insert", "insertó");
			verificacion = true;
		} catch (Exception e) {
			e.printStackTrace();
			verificacion = false;
		}
		return verificacion;
	}

	/**
	 * Modificar el registro
	 */
	public long updateUsuario(ContentValues reg) {
		long result = 0;

		// if (db == null)
		// abrir();

		if (reg.containsKey(CU_ID_LOCAL)) {
			//
			// Obtenemos el id y lo borramos de los valores
			//
			long id = reg.getAsLong(CU_ID_LOCAL);

			reg.remove(CU_ID_LOCAL);

			//
			// Actualizamos el registro con el identificador que hemos extraido
			//
			result = db.update(TABLE_USUARIO, reg, "id_local=" + id, null);
		}
		return result;
	}

	public void deleteUsuario(long idLocalUsuario) {

		db.delete(TABLE_USUARIO, "id_local=? ", new String[] { "" + idLocalUsuario});

	}

	public List<UsuarioVO> getListUsuario(String sql) {

		Cursor c = db.rawQuery(sql, null);// se coloca null por no le
											// colocamos ? al where

		List<UsuarioVO> listUsuario= null;
		c.moveToFirst();
		if (c.getCount() > 0) {
			listUsuario= new ArrayList<UsuarioVO>();
			while (!c.isAfterLast()) {
				UsuarioVO usuario= new UsuarioVO();				

				usuario.setId_local((long)c.getInt(0));
				usuario.setId_server((long)c.getInt(1));
				usuario.setNombre(c.getString(2));
				usuario.setCedula(c.getInt(3));

				usuario.setApellidos(c.getString(4));
				usuario.setCorreo(c.getString(5));
				usuario.setPassword(c.getString(6));
				usuario.setSaldo((long)c.getInt(7));			

				usuario.setCookie(c.getString(8));
				usuario.setId_n_push(c.getString(9));
				
				listUsuario.add(usuario);
				c.moveToNext();
			}
		}
		return listUsuario;

	}

	public List<UsuarioVO> getAllUsuario() {
		String sql = "SELECT * FROM " + TABLE_USUARIO;
		return getListUsuario(sql);

	}

	public List<UsuarioVO> searchUserByName(String name) {

		String sql = "SELECT * FROM " + TABLE_USUARIO + " WHERE " + CU_NOMBRE+ " LIKE  '" + name + "' ";

		// + " AND " + FormularioDBHelper.CLIENT_COLUMN_LOGIN + " LIKE  '"
		// + name + "' ";
		Log.i("mensajeDBADAPTER sql", sql);
		return getListUsuario(sql);
	}
	
	public List<UsuarioVO> searchUserByCedula(int cedula) {

		String sql = "SELECT * FROM " + TABLE_USUARIO + " WHERE " + CU_CEDULA+ " LIKE  '" + cedula+ "' ";

		// + " AND " + FormularioDBHelper.CLIENT_COLUMN_LOGIN + " LIKE  '"
		// + name + "' ";
		Log.i("mensajeDBADAPTER sql", sql);
		return getListUsuario(sql);
	}

	public List<UsuarioVO> searchUsuarioByCedulaPass(int cedula, String password) {

		String sql = "SELECT * FROM " + TABLE_USUARIO+ " WHERE " + CU_CEDULA + " LIKE  '" + cedula + "' "

		+ " AND " + CU_PASSWORD + " LIKE  '" + password+ "' ";
		Log.i("mensajeDBADAPTER sql", sql);
		return getListUsuario(sql);
	}

	public UsuarioVO getUsuarioById(long id_server) {

		UsuarioVO usuario = null;

		String sql = "SELECT * FROM "

//		+ TABLE_USUARIO + " WHERE _id=?";
				+ TABLE_USUARIO + " WHERE id_server=?";

		String[] arg = { "" + id_server };

		Cursor c = db.rawQuery(sql, arg);// me devuelve lo obtenido con la
											// sentencia sql y el id

		// establece los valores obenitdos del cursosr en la isntanciación de
		// Food
		if (c.getCount() > 0) {
			c.moveToNext();

			usuario = new UsuarioVO();

			usuario.setId_local((long)c.getInt(0));
			usuario.setId_server((long)c.getInt(1));
			usuario.setNombre(c.getString(2));
			usuario.setCedula(c.getInt(3));

			usuario.setApellidos(c.getString(4));
			usuario.setCorreo(c.getString(5));
			usuario.setPassword(c.getString(6));
			usuario.setSaldo((long)c.getInt(7));	
			
			usuario.setCookie(c.getString(8));
			usuario.setId_n_push(c.getString(9));

		}

		return usuario;
	}
	
	public UsuarioVO getUsuarioByCed(long cedula) {

		UsuarioVO usuario = null;

		String sql = "SELECT * FROM "

//		+ TABLE_USUARIO + " WHERE _id=?";
				+ TABLE_USUARIO + " WHERE cedula=?";

		String[] arg = { "" + cedula };

		Cursor c = db.rawQuery(sql, arg);// me devuelve lo obtenido con la
											// sentencia sql y el id

		// establece los valores obenitdos del cursosr en la isntanciación de
		// Food
		if (c.getCount() > 0) {
			c.moveToNext();

			usuario = new UsuarioVO();

			usuario.setId_local((long)c.getInt(0));
			usuario.setId_server((long)c.getInt(1));
			usuario.setNombre(c.getString(2));
			usuario.setCedula(c.getInt(3));

			usuario.setApellidos(c.getString(4));
			usuario.setCorreo(c.getString(5));
			usuario.setPassword(c.getString(6));
			usuario.setSaldo((long)c.getInt(7));	
			
			usuario.setCookie(c.getString(8));
			usuario.setId_n_push(c.getString(9));

		}

		return usuario;
	}
	
	
}

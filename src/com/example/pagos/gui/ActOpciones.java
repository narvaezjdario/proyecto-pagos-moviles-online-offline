package com.example.pagos.gui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pagos.R;
import com.example.pagos.data.controller.PagosDBController;
import com.example.pagos.networking.HttpAsyncTask;
import com.example.pagos.networking.HttpAsyncTask.HttpInterface;
import com.example.pagos.vo.Codigos;
import com.example.pagos.vo.UsuarioVO;

public class ActOpciones extends Activity implements OnClickListener,
		android.content.DialogInterface.OnClickListener, HttpInterface {

	private Menu menu;
	Button btnCobrar,btnPagar ,btnTransferir, btnInfo;

	View cerrarSView;
	TextView tvCerrarS, tvNombreU, tvSaldo;
	AlertDialog alertCerrarS;

	View fCobrarView;
	TextView tvTotalCobro, tvVlrTrans, tvTleVlrTrans;
	EditText eTotalCobro;
	AlertDialog alertForCobro;

	long idServer;
	PagosDBController data;
	public static final String VALOR_A_COBRAR = "vlr_cobro";

	public static String cookieValue;
	public static String cedulaSCobrador;
	public static String cedulaSCliente;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ly_act_opciones);
		this.setTitle("");

		data = new PagosDBController(this);
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		casting();

		idServer = extras.getLong(ActInicioSesion.ID_SERVER);
		UsuarioVO dataUser = data.getUsuarioById(idServer);
		tvNombreU.setText("Usuario: " + dataUser.getNombre());
		tvSaldo.setText("Tu saldo es: " + dataUser.getSaldo());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		menu.clear();
		this.menu = menu;
		fijarMenu(1);

		return true;
	}

	public void fijarMenu(int estado) {

		Log.i("fijarmenu", "inicio");
		if (menu != null) {
			menu.clear();

			MenuInflater inflater = getMenuInflater();
			switch (estado) {
			case 1:
				Log.i("fijarmenu", "1");
				inflater.inflate(R.menu.menu_opciones, menu);
				break;

			}

		}
	}

	public void casting() {

		tvNombreU = (TextView) findViewById(R.id.tv_nombre_usuario);
		tvSaldo = (TextView) findViewById(R.id.tv_saldo);

		// Alert Cerrar Sesion
		cerrarSView = View.inflate(this, R.layout.al_di_cerrar_sesion, null);
		alertCerrarS = getAlertDialog(R.string.cerrar, cerrarSView);

		// Alert Formulario de cobro
		fCobrarView = View.inflate(this, R.layout.al_di_formulario_cobro, null);
		tvTotalCobro = (TextView) fCobrarView.findViewById(R.id.tv_total_cobro);
		eTotalCobro = (EditText) fCobrarView.findViewById(R.id.e_total_cobro);
		tvVlrTrans = (TextView) fCobrarView
				.findViewById(R.id.tv_title_transaccion);
		tvTleVlrTrans = (TextView) fCobrarView
				.findViewById(R.id.tv_vlr_transaccion);

		alertForCobro = getAlertDialog(R.string.formulario_cobro, fCobrarView);

		btnCobrar = (Button) findViewById(R.id.btn_cobrar);
		btnPagar =  (Button) findViewById(R.id.btn_pagar);
		btnTransferir = (Button) findViewById(R.id.btn_transferir);
		btnInfo = (Button) findViewById(R.id.btn_infor);

		btnCobrar.setOnClickListener(this);
		btnPagar.setOnClickListener(this);
		btnTransferir.setOnClickListener(this);
		btnInfo.setOnClickListener(this);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.cerrar_sesion:
			alertCerrarS.show();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public AlertDialog getAlertDialog(int idTitle, View content) {

		// dialog add edit
		AlertDialog alertdialog = new AlertDialog.Builder(this)
				.setTitle(idTitle).setView(content)
				.setPositiveButton(R.string.si, this)
				.setNegativeButton(R.string.no, this).create();
		return alertdialog;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {

		if (which == DialogInterface.BUTTON_POSITIVE) {

			if (dialog == alertCerrarS) {
				enviarJSonCerrarSesion();
				// Intent iCerrarS= new Intent(this,ActInicioSesion.class);
				// startActivity(iCerrarS);
			} else if (dialog == alertForCobro) {
				getInfoCobro();// l�gica para captura de infor. del formulario e
								// ir a Actpagar
			}

		}

	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btn_cobrar:
//			enviarJsonCobrar();
			alertForCobro.show();
			break;
			
		case R.id.btn_pagar:
//			enviarJSONPagar();
			UsuarioVO user = data.getUsuarioByCed(ActInicioSesion.cedulaI);
			cookieValue = user.getCookie();
			cedulaSCobrador = String.valueOf(user.getCedula());
			cedulaSCliente = String.valueOf(user.getCedula());
			Intent iPagar = new Intent (this,ActPagar.class);
			startActivity(iPagar);
			break;

		case R.id.btn_transferir:
			Intent iTransferir = new Intent(this, ActTransferir.class);
			startActivity(iTransferir);
			break;
		case R.id.btn_infor:
			Intent iInfo = new Intent(this, ActInfoCuenta.class);
			startActivity(iInfo);
			break;

		default:
			break;
		}

	}

	public void enviarJsonCobrar() {

		List<NameValuePair> dataPost = new ArrayList<NameValuePair>();

		NameValuePair n1 = new BasicNameValuePair("cedula",
				ActInicioSesion.cedulaI + "");
		dataPost.add(n1);

		// Extraer la cookie de la BD
		UsuarioVO user = data.getUsuarioByCed(ActInicioSesion.cedulaI);
		cookieValue = user.getCookie();
		cedulaSCobrador = String.valueOf(user.getCedula());
		cedulaSCliente = String.valueOf(user.getCedula());
		
		HttpAsyncTask task = new HttpAsyncTask(this, dataPost,
				Codigos.OPERACION_PAGAR);

		String direccionPagar = getResources().getString(R.string.dx_base)
				+ getResources().getString(R.string.dx_action_pagar);
		task.execute(direccionPagar);

	}
	
	
	

	public void enviarJSonCerrarSesion() {
		List<NameValuePair> dataPost = new ArrayList<NameValuePair>();
		// NameValuePair n1 = new BasicNameValuePair("cedula",
		// ActInicioSesion.cedulaI + "");
		// dataPost.add(n1);
		// Extraer la cookie de la BD
		UsuarioVO user = data.getUsuarioByCed(ActInicioSesion.cedulaI);
		cookieValue = user.getCookie();

		HttpAsyncTask task = new HttpAsyncTask(this, dataPost,
				Codigos.OPERACION_CERRAR_SESION);

		String direccionCerrarSesion = getResources().getString(
				R.string.dx_base)
				+ getResources().getString(R.string.dx_cerrar_sesion);
		task.execute(direccionCerrarSesion);
	}

	public void getInfoCobro() {
		
		// Extraer la cookie de la BD
				UsuarioVO user = data.getUsuarioByCed(ActInicioSesion.cedulaI);
				cookieValue = user.getCookie();
				cedulaSCobrador = String.valueOf(user.getCedula());
				cedulaSCliente = String.valueOf(user.getCedula());
		
		//M�todo para la captura de la informaci�n a pagar
		String vlrCobroS =eTotalCobro.getText().toString();
		float vlrPagoF;
		if(vlrCobroS.length()==0){
			Toast.makeText(this, R.string.completar_informacion, Toast.LENGTH_SHORT).show();
			alertForCobro.show();
		}
		else{
			if(vlrCobroS.equals("0")){
				Toast.makeText(this, R.string.vlr_pago_incorrecto, Toast.LENGTH_SHORT).show();
				alertForCobro.show();
			}
			else{
				vlrPagoF = Float.valueOf(vlrCobroS);
				Intent iPagar = new Intent(this, ActCobrar.class);
				iPagar.putExtra(VALOR_A_COBRAR, vlrPagoF);
				startActivity(iPagar);				
				
			}
			
			
		}		
		
		Float vlrPago= Float.valueOf(eTotalCobro.getText().toString());		

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK)
			return false;

		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void processRta(String rta) {
		JSONObject object;
		try {
			object = new JSONObject(rta);

			if (object.has("noSession")) {
				Toast.makeText(this, R.string.sesion_no_disponible,
						Toast.LENGTH_SHORT).show();
				Intent iCerrarS = new Intent(this, ActInicioSesion.class);
				startActivity(iCerrarS);
			}

			else if (object.has("noPermissions")) {
				Toast.makeText(this, R.string.sin_permisos, Toast.LENGTH_SHORT)
						.show();
			}

			else if (object.has("closeSession")) {
				Toast.makeText(this, R.string.cierre_de_sesion,
						Toast.LENGTH_SHORT).show();
				Intent iCerrarS = new Intent(this, ActInicioSesion.class);
				startActivity(iCerrarS);
			}

			else {
				// verFormularioPago();
				alertForCobro.show();

			}

		} catch (JSONException e) {
			e.printStackTrace();

		}

	}
	
	//Para Pagos NFC....

}

package com.example.pagos.gui;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentFilter.MalformedMimeTypeException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.pagos.R;

@SuppressLint("NewApi")
public class ActCobrar extends Activity implements
		android.content.DialogInterface.OnClickListener, OnClickListener {

	// Alerta para iniciar nfc
	View pagarView;
	AlertDialog alertPagar;

	Button btnCobrar;

	// /NFC Para descurbrir si tiene NFC
	private NdefMessage mNdefPushMessage;
	private NfcAdapter mAdapter;
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	
	//Para la txt de la info via NFC
	private static final String TAG = "notasadhesivas";
	private boolean mResumed = false;
	private boolean mWriteMode = false;	
	EditText mNote;
	PendingIntent mNfcPendingIntent;
	IntentFilter[] mWriteTagFilters;
	IntentFilter[] mNdefExchangeFilters;
	

	float vlrPagoF; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ly_act_cobrar);
		this.setTitle("Cobrar");
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		vlrPagoF = extras.getFloat(ActOpciones.VALOR_A_COBRAR);
	
		casting();
		initNFC();
//		escribirNFC();
		
	}
	

	public void casting() {

		pagarView = View.inflate(this, R.layout.al_di_iniciar_nfc, null);
		alertPagar = getAlertDialog(R.string.pagar, pagarView);

//		btnCobrar = (Button) findViewById(R.id.btn_cobrar);
//		btnCobrar.setOnClickListener(this);

	}

	public AlertDialog getAlertDialog(int idTitle, View content) {

		// dialog add edit
		AlertDialog alertdialog = new AlertDialog.Builder(this)
				.setTitle(idTitle).setView(content)
				.setPositiveButton(R.string.si, this)
				.setNegativeButton(R.string.no, this).create();
		return alertdialog;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {

		if (which == DialogInterface.BUTTON_POSITIVE) {

			if (dialog == alertPagar) {

			}
		}

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_cobrar:
			mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
			if (mNfcAdapter != null) {
				Log.i("nfc", "si se puede leer");
//				Toast.makeText(this,
//						getResources().getString(R.string.leertag),
//						Toast.LENGTH_LONG).show();
//				escribirNFC();
				// iniSmartTag();
			} else {
				Log.i("nfc", "NO SE PUEDE NFC");
				Toast.makeText(this,
						getResources().getString(R.string.nosepuedeleertag),
						Toast.LENGTH_LONG).show();
			}
			// alertPagar.show();
			break;

		default:
			break;
		}
	}
	
public void initNFC(){
		
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
//		mNote = ((EditText) findViewById(R.id.note));
//		findViewById(R.id.write_tag).setOnClickListener(mTagWriter);
//		mNote.addTextChangedListener(mTextWatcher);
		// Gestiona los intents NFC en esta actividad.
		mNfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

		// Intent filters para leer el mensaje de una etiqueta o intercambiarlo
		// sobre p2p.
		IntentFilter ndefDetected = new IntentFilter(
				NfcAdapter.ACTION_NDEF_DISCOVERED);
		try {
			ndefDetected.addDataType("text/plain");
		} catch (MalformedMimeTypeException e) {
		}
		mNdefExchangeFilters = new IntentFilter[] { ndefDetected };

		// Intentfilters para escribir una etiqueta.
//		IntentFilter tagDetected = new IntentFilter(
//				NfcAdapter.ACTION_TAG_DISCOVERED);
//		mWriteTagFilters = new IntentFilter[] { tagDetected };
		
		
	}

//m�todo heredado de la implementaci�n del oncreate
	@Override
	protected void onNewIntent(Intent intent) {
		 super.onNewIntent(intent);

		// Modo de intercambio NDEF
		// Lee el mensaje y lo muestra
//		if (!mWriteMode
//				&& NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
//			NdefMessage[] msgs = getNdefMessages(intent);
//			promptForContent(msgs[0]);
//		}

		// Modo de escritura de etiquetas
		if (mWriteMode
				&& NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
			Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			writeTag(getNoteAsNdef(), detectedTag);
		}
	}

	// m�todo heredado de la implementaci�n del oncreate
	@SuppressWarnings("deprecation")
	@Override
	protected void onPause() {
		super.onPause();
		mResumed = false;
		mNfcAdapter.disableForegroundNdefPush(this);
	}

	// m�todo heredado de la implementaci�n del oncreate
	@Override
	protected void onResume() {
		super.onResume();
		mResumed = true;
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
			NdefMessage[] messages = getNdefMessages(getIntent());
			byte[] payload = messages[0].getRecords()[0].getPayload();
			setNoteBody(new String(payload));
			setIntent(new Intent());
		}
		enableNdefExchangeMode();
	}

	// Se obtiene la informaci�n (msg) del intent
	@SuppressLint("NewApi")
	// @SuppressLint("NewApi")
	NdefMessage[] getNdefMessages(Intent intent) {
		// Procesamiento del intent
		NdefMessage[] msgs = null;
		String action = intent.getAction();
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
				|| NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
			Parcelable[] rawMsgs = intent
					.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			if (rawMsgs != null) {
				msgs = new NdefMessage[rawMsgs.length];
				for (int i = 0; i < rawMsgs.length; i++) {
					msgs[i] = (NdefMessage) rawMsgs[i];
				}
			} else {
				// Tipo de etiquetadesconocido
				byte[] empty = new byte[] {};
				NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN,
						empty, empty, empty);
				NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
				msgs = new NdefMessage[] { msg };
			}
		} else {
			Log.d(TAG, "Unknown intent.");
			finish();
		}
		return msgs;
	}

	private void promptForContent(final NdefMessage msg) {

		new AlertDialog.Builder(this)
				.setTitle("�Reemplazar el contenido actual? ")
				.setPositiveButton("Si", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {

						String body = new String(msg.getRecords()[0]
								.getPayload());
						setNoteBody(body);
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
					}
				}).show();
	}

	private void setNoteBody(String body) {
		Editable text = mNote.getText();
		text.clear();
		text.append(body);
	}

	// M�todo enableNdefExchangeMode
	private void enableNdefExchangeMode() {
		mNfcAdapter.enableForegroundNdefPush(ActCobrar.this,
				getNoteAsNdef());
		mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent,
				mNdefExchangeFilters, null);
	}

	// M�todo getNoteAsNdef:
	private NdefMessage getNoteAsNdef() {
		
		String cedulaCobrador = ActOpciones.cedulaSCobrador;
//		byte[] textBytes = mNote.getText().toString().getBytes();		
		byte[] textBytes = String.valueOf(vlrPagoF+"-"+cedulaCobrador).getBytes();
		NdefRecord textRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
				"text/plain".getBytes(), new byte[] {}, textBytes);
		return new NdefMessage(new NdefRecord[] { textRecord });
	}

//	 M�todos para la escritura
	private View.OnClickListener mTagWriter = new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			// Escribe la etiqueta mientras se muestra el di�logo.
			disableNdefExchangeMode();
			enableTagWriteMode();
			new AlertDialog.Builder(ActCobrar.this)
					.setTitle("Toque la etiqueta a escribir ")
					.setOnCancelListener(
							new DialogInterface.OnCancelListener() {
								@Override
								public void onCancel(DialogInterface dialog) {
									disableTagWriteMode();
									enableNdefExchangeMode();
								}
							}).create().show();
		}
	};
//	public void escribirNFC(){
//		disableNdefExchangeMode();
//		enableTagWriteMode();
//		new AlertDialog.Builder(ActCobrar.this)
//		.setTitle("Acerque el dispositivo del usuario")
//		.setOnCancelListener(
//				new DialogInterface.OnCancelListener() {
//					@Override
//					public void onCancel(DialogInterface dialog) {
//						disableTagWriteMode();
//						enableNdefExchangeMode();
//					}
//				}).create().show();
//		
//	}

	private TextWatcher mTextWatcher = new TextWatcher() {
		public void onTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {
		}

		@Override
		public void afterTextChanged(Editable arg0) {
			if (mResumed) {
				mNfcAdapter.enableForegroundNdefPush(
						ActCobrar.this, getNoteAsNdef());
			}
		}
	};

	boolean writeTag(NdefMessage message, Tag tag) {
		int size = message.toByteArray().length;
		try {
			Ndef ndef = Ndef.get(tag);
			if (ndef != null) {
				ndef.connect();
				if (!ndef.isWritable()) {
					toast("La etiqueta es de s�lo lectura.");
					return false;
				}
				if (ndef.getMaxSize() < size) {
					toast("La capacidad de la etiqueta es de "
							+ ndef.getMaxSize() + " bytes, el mensaje es de "
							+ size + " bytes.");
					return false;
				}
				ndef.writeNdefMessage(message);
				toast("Mensaje escrito sobre la etiqueta pre formateada.");
				return true;
			} else {
				NdefFormatable format = NdefFormatable.get(tag);
				if (format != null) {
					try {
						format.connect();
						format.format(message);
						toast("Etiquetaformateada y mensaje escrito.");
						return true;
					} catch (IOException e) {
						toast("Formato de etiquetafallido.");
						return false;
					}
				} else {
					toast("La etiqueta no soporta NDEF.");
					return false;
				}
			}
		} catch (Exception e) {
			toast("Fall� proceso de escritura.");
		}
		return false;
	}

	private void disableNdefExchangeMode() {
		mNfcAdapter.disableForegroundNdefPush(this);
		mNfcAdapter.disableForegroundDispatch(this);
	}

	private void enableTagWriteMode() {
		mWriteMode = true;
		IntentFilter tagDetected = new IntentFilter(
				NfcAdapter.ACTION_TAG_DISCOVERED);
		mWriteTagFilters = new IntentFilter[] { tagDetected };
		mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent,
				mWriteTagFilters, null);
	}

	private void disableTagWriteMode() {
		mWriteMode = false;
		mNfcAdapter.disableForegroundDispatch(this);
	}

	private void toast(String text) {
		Toast.makeText(this, text, Toast.LENGTH_SHORT).show();

	}
}

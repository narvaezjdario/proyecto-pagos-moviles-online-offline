package com.example.pagos.gui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.widget.Toast;

public class ActSetCookies extends Activity{
	
	
	//Para guardar en el preference
	static final String NAME_PREFERENCE="preferencias";

	SharedPreferences preferences;
	Editor editor;
	
	String info;
	
	
	
	public ActSetCookies(String info) {
		super();
		this.info=info;
	}



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
				
//		inicialización de los preferences
		preferences= getSharedPreferences(NAME_PREFERENCE, 0);
		
		String info=preferences.getString("dato", "Ninguno");//traer la informacion.
//		txt.setText(info);		
		editor= preferences.edit();
		
		String newInfo= "hola";//edit.getText().toString();
		editor.putString("dato", newInfo);
		editor.commit();
		Toast.makeText(this, "Dato Almacenado"
				, Toast.LENGTH_SHORT).show();
	
		
	}

}

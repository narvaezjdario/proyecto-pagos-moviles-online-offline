package com.example.pagos.gui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pagos.R;
import com.example.pagos.networking.HttpAsyncTask;
import com.example.pagos.networking.HttpAsyncTask.HttpInterface;
import com.example.pagos.vo.Codigos;

public class ActPagar extends Activity implements HttpInterface{
	
	private static final String TAG = "notasadhesivas";
	private boolean mResumed = false;
	private boolean mWriteMode = false;
	NfcAdapter mNfcAdapter;
	EditText mNote;
	PendingIntent mNfcPendingIntent;
	IntentFilter[] mWriteTagFilters;
	IntentFilter[] mNdefExchangeFilters;
	
	String cedCobrador;
	String cedCliente;
	String TotPago;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ly_act_pagar);
	
		this.setTitle("Pagar");
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mNote = (EditText) findViewById(R.id.e_ctdad_pagar);
//		findViewById(R.id.write_tag).setOnClickListener(mTagWriter);
//		mNote.addTextChangedListener(mTextWatcher);
		// Gestiona los intents NFC en esta actividad.
		mNfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

//		enviarJSONPagar();
		
		// Intent filters para leer el mensaje de una etiqueta o intercambiarlo
		// sobre p2p.
//		IntentFilter ndefDetected = new IntentFilter(
//				NfcAdapter.ACTION_NDEF_DISCOVERED);
//		try {
//			ndefDetected.addDataType("text/plain");
//		} catch (MalformedMimeTypeException e) {
//		}
//		mNdefExchangeFilters = new IntentFilter[] { ndefDetected };

		// Intentfilters para escribir una etiqueta.
//		IntentFilter tagDetected = new IntentFilter(
//				NfcAdapter.ACTION_TAG_DISCOVERED);
//		mWriteTagFilters = new IntentFilter[] { tagDetected };
		
	}
	
	
	// m�todo heredado de la implementaci�n del oncreate
		@Override
		protected void onNewIntent(Intent intent) {
			 super.onNewIntent(intent);

			// Modo de intercambio NDEF
			// Lee el mensaje y lo muestra
			if (!mWriteMode
					&& NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
				NdefMessage[] msgs = getNdefMessages(intent);
				promptForContent(msgs[0]);
			}

			// Modo de escritura de etiquetas
//			if (mWriteMode
//					&& NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
//				Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
//				writeTag(getNoteAsNdef(), detectedTag);
//			}
		}

		// m�todo heredado de la implementaci�n del oncreate
		@SuppressWarnings("deprecation")
		@Override
		protected void onPause() {
			super.onPause();
			mResumed = false;
			mNfcAdapter.disableForegroundNdefPush(this);
		}

		// m�todo heredado de la implementaci�n del oncreate
		@Override
		protected void onResume() {
			super.onResume();
			mResumed = true;
			if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
				NdefMessage[] messages = getNdefMessages(getIntent());
				byte[] payload = messages[0].getRecords()[0].getPayload();
				setNoteBody(new String(payload));
				setIntent(new Intent());
			}
			enableNdefExchangeMode();
		}

		// Se obtiene la informaci�n (msg) del intent
		@SuppressLint("NewApi")
		// @SuppressLint("NewApi")
		NdefMessage[] getNdefMessages(Intent intent) {
			// Procesamiento del intent
			NdefMessage[] msgs = null;
			String action = intent.getAction();
			if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
					|| NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
				Parcelable[] rawMsgs = intent
						.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
				if (rawMsgs != null) {
					msgs = new NdefMessage[rawMsgs.length];
					for (int i = 0; i < rawMsgs.length; i++) {
						msgs[i] = (NdefMessage) rawMsgs[i];
					}
				} else {
					// Tipo de etiquetadesconocido
					byte[] empty = new byte[] {};
					NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN,
							empty, empty, empty);
					NdefMessage msg = new NdefMessage(new NdefRecord[] { record });
					msgs = new NdefMessage[] { msg };
				}
			} else {
				Log.d(TAG, "Unknown intent.");
				finish();
			}
			return msgs;
		}

		private void promptForContent(final NdefMessage msg) {

			new AlertDialog.Builder(this)
					.setTitle("�Desea efectuar este pago? ")
					.setPositiveButton("Si", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface arg0, int arg1) {

							String body = new String(msg.getRecords()[0]
									.getPayload());
							setNoteBody(body);
						}
					})
					.setNegativeButton("No", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface arg0, int arg1) {
						}
					}).show();
		}
//Donde se recupera la informaci�n entrante
		private void setNoteBody(String body) {
			Editable text = mNote.getText();
			text.clear();
			text.append(body);
			mNote.setText(body);
			toast(body);
			String delimiter="-";
			String []palabras;
			palabras = body.split(delimiter);
//			recupero la cedula y el monto a pagar
			for (int i = 0; i < palabras.length; i++) {
				setLog("infopago", palabras[i]);
			}
			enviarJSONPagar();
		}

		// M�todo enableNdefExchangeMode
		private void enableNdefExchangeMode() {
			mNfcAdapter.enableForegroundNdefPush(ActPagar.this,
					getNoteAsNdef());
			mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent,
					mNdefExchangeFilters, null);
		}

		// M�todo getNoteAsNdef:
		private NdefMessage getNoteAsNdef() {
			byte[] textBytes = mNote.getText().toString().getBytes();
			NdefRecord textRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
					"text/plain".getBytes(), new byte[] {}, textBytes);
			return new NdefMessage(new NdefRecord[] { textRecord });
		}

//		// M�todos para la escritura
//		private View.OnClickListener mTagWriter = new View.OnClickListener() {
//			@Override
//			public void onClick(View arg0) {
//				// Escribe la etiqueta mientras se muestra el di�logo.
//				disableNdefExchangeMode();
//				enableTagWriteMode();
//				new AlertDialog.Builder(ActPagar.this)
//						.setTitle("Toque la etiqueta a escribir ")
//						.setOnCancelListener(
//								new DialogInterface.OnCancelListener() {
//									@Override
//									public void onCancel(DialogInterface dialog) {
//										disableTagWriteMode();
//										enableNdefExchangeMode();
//									}
//								}).create().show();
//			}
//		};

//		private TextWatcher mTextWatcher = new TextWatcher() {
//			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
//					int arg3) {
//			}
//
//			@Override
//			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//					int arg3) {
//			}
//
//			@Override
//			public void afterTextChanged(Editable arg0) {
//				if (mResumed) {
//					mNfcAdapter.enableForegroundNdefPush(
//							ActPagar.this, getNoteAsNdef());
//				}
//			}
//		};

//		boolean writeTag(NdefMessage message, Tag tag) {
//			int size = message.toByteArray().length;
//			try {
//				Ndef ndef = Ndef.get(tag);
//				if (ndef != null) {
//					ndef.connect();
//					if (!ndef.isWritable()) {
//						toast("La etiqueta es de s�lo lectura.");
//						return false;
//					}
//					if (ndef.getMaxSize() < size) {
//						toast("La capacidad de la etiqueta es de "
//								+ ndef.getMaxSize() + " bytes, el mensaje es de "
//								+ size + " bytes.");
//						return false;
//					}
//					ndef.writeNdefMessage(message);
//					toast("Mensaje escrito sobre la etiqueta pre formateada.");
//					return true;
//				} else {
//					NdefFormatable format = NdefFormatable.get(tag);
//					if (format != null) {
//						try {
//							format.connect();
//							format.format(message);
//							toast("Etiquetaformateada y mensaje escrito.");
//							return true;
//						} catch (IOException e) {
//							toast("Formato de etiquetafallido.");
//							return false;
//						}
//					} else {
//						toast("La etiqueta no soporta NDEF.");
//						return false;
//					}
//				}
//			} catch (Exception e) {
//				toast("Fall� proceso de escritura.");
//			}
//			return false;
//		}

		private void disableNdefExchangeMode() {
			mNfcAdapter.disableForegroundNdefPush(this);
			mNfcAdapter.disableForegroundDispatch(this);
		}

//		private void enableTagWriteMode() {
//			mWriteMode = true;
//			IntentFilter tagDetected = new IntentFilter(
//					NfcAdapter.ACTION_TAG_DISCOVERED);
//			mWriteTagFilters = new IntentFilter[] { tagDetected };
//			mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent,
//					mWriteTagFilters, null);
//		}

//		private void disableTagWriteMode() {
//			mWriteMode = false;
//			mNfcAdapter.disableForegroundDispatch(this);
//		}

		
		
		
		//Enviar JSON cobrar 
		
	
		
		
		private void toast(String text) {
			Toast.makeText(this, text, Toast.LENGTH_SHORT).show();

		}
		public void setLog(String tag,String text){
			Log.i(tag, text);
			
		}


		public void enviarJSONPagar(){
			
			List<NameValuePair> dataPost= new ArrayList<NameValuePair>();			
			
//			NameValuePair n1= new BasicNameValuePair("cookie_cliente"   , ActOpciones.cookieValue);		
			NameValuePair n1= new BasicNameValuePair("cedula_vendedor"   , "123");		
			NameValuePair n2= new BasicNameValuePair("valor_pago", "1000");				
			
			dataPost.add(n1);
			dataPost.add(n2);

			HttpAsyncTask task = new HttpAsyncTask(this, dataPost, Codigos.OPERACION_PAGAR);

			String direccionLogin = getResources().getString(R.string.dx_base)
					+ getResources().getString(R.string.dx_pago);

			task.execute(direccionLogin);
			
		}
		@Override
		public void processRta(String rta) {
			
			
		}
		

}

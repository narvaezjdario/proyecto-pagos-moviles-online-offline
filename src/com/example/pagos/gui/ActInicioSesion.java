package com.example.pagos.gui;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.cookie.Cookie;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pagos.R;
import com.example.pagos.data.controller.PagosDBController;
import com.example.pagos.networking.HttpAsyncTask;
import com.example.pagos.networking.HttpAsyncTask.HttpInterface;
import com.example.pagos.networking.HttpConnection;
import com.example.pagos.vo.Codigos;
import com.example.pagos.vo.UsuarioVO;

public class ActInicioSesion extends Activity implements OnClickListener,
		HttpInterface {
	private static final int HELLO_ID = 1;
	private Cursor cursorU;
	PagosDBController data;

	EditText eCedula, ePass;
	Button btnISesion, btnRegistrar;

	public static int cedulaI;
	public static long idServer;
	public static long saldoF;
	String passS;

	public static final String ID_SERVER = "id_server";
	 private ProgressDialog indicadorProgreso;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ly_act_inicio_sesion);
		this.setTitle("");
		data = new PagosDBController(this);

		// UsuarioVO usuario =data.getUsuarioById(1);

		casting();
//		setVibrar();
	}

	public void casting() {

		eCedula = (EditText) findViewById(R.id.e_cedula);
		ePass = (EditText) findViewById(R.id.e_password);

		btnISesion = (Button) findViewById(R.id.btn_iniciar_sesion);
		btnRegistrar = (Button) findViewById(R.id.btn_registrarse);

		btnISesion.setOnClickListener(this);
		btnRegistrar.setOnClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.act_inicio_sesion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btn_iniciar_sesion:
			if (isNetworkAvailable(ActInicioSesion.this)) {
				indicadorProgreso = new ProgressDialog(this).show(this, getString(R.string.sincronizando_titulo), getString(R.string.sincronizando_sesion));
				verificarUsuario();
				enviarJson();
			} else {
				// crearUsuarioBDLocal(-1, NUEVO_U_BD_LOCAL);
				// verificarUsuario();
				Toast.makeText(this, R.string.red_no_disponible,
						Toast.LENGTH_SHORT).show();
			}

			break;
		case R.id.btn_registrarse:
			Intent iRegistro = new Intent(this, ActRegistro.class);
			startActivity(iRegistro);
			break;

		default:
			break;
		}

	}

	public void verificarUsuario() {

		if (eCedula.getText().toString().length() != 0
				&& ePass.getText().toString().length() != 0) {

			cedulaI = Integer.parseInt(eCedula.getText().toString());
			passS = ePass.getText().toString();

			List<UsuarioVO> listUsuario = data.searchUsuarioByCedulaPass(
					cedulaI, passS);

			if (listUsuario == null) {
				// Toast.makeText(this, R.string.usuario_no_encontrado,
				// Toast.LENGTH_SHORT).show();
				ePass.setText("");
			} else {

				eCedula.setText("");
				ePass.setText("");
				Object oU = listUsuario.get(0);
				UsuarioVO usuario = (UsuarioVO) oU;
				Log.i("nombre", usuario.getNombre() + "");
				// Intent iISesion = new Intent(this,ActOpciones.class);
				// startActivity(iISesion);
			}
		} else {
			Toast.makeText(this, R.string.completar_informacion,
					Toast.LENGTH_SHORT).show();
		}

	}

	public void enviarJson() {

		List<NameValuePair> dataPost = new ArrayList<NameValuePair>();

		NameValuePair n1 = new BasicNameValuePair("cedula", cedulaI + "");
		NameValuePair n2 = new BasicNameValuePair("password", passS + "");

		dataPost.add(n1);
		dataPost.add(n2);

		HttpAsyncTask task = new HttpAsyncTask(this, dataPost, Codigos.OPERACION_LOGIN);

		String direccionLogin = getResources().getString(R.string.dx_base)
				+ getResources().getString(R.string.dx_login);

		task.execute(direccionLogin);

	}

	public static boolean isNetworkAvailable(Context context) {

		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public void guardarCookies() {

		// 1. Debo traer al usuario de la BD local
		if(data.getUsuarioByCed(cedulaI)!=null){
		UsuarioVO user = data.getUsuarioByCed(cedulaI);
		Cookie sessionInfo = HttpConnection.sessionInfo;
		Log.i("info", sessionInfo.getValue() + "");

		// adiciono los valores a actualizar en el registro
		ContentValues reg = new ContentValues();
		reg.put(data.CU_ID_LOCAL, user.getId_local());
		reg.put(data.CU_SALDO, saldoF);
		reg.put(data.CU_COOKIE, sessionInfo.getValue() + "");

		// actualizo la tbl
		data.updateUsuario(reg);
		Intent intent = new Intent(this, ActOpciones.class);
		intent.putExtra(ID_SERVER, idServer);
		startActivity(intent);
		}
		// verficar la actualización
		// UsuarioVO user2 = data.getUsuarioByCed(cedulaI);

	}

	@Override
	public void processRta(String rta) {

		JSONObject object;
		try {
			object = new JSONObject(rta);
			boolean logueo = Boolean.parseBoolean(object.getString("logueo"));

			if (logueo) {
				idServer = Long.parseLong(object.getString("id_server"));
				saldoF = Long.parseLong(object.getString("saldo"));
				guardarCookies();
				indicadorProgreso.cancel();
				
			} else {
				indicadorProgreso.cancel();
				Toast.makeText(this, R.string.usuario_no_encontrado,
						Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
			indicadorProgreso.cancel();
		}

	}
	
	public void setVibrar(){
		// 1. Get a reference to the NotificationManager
		String ns = Context.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);
		// 2. Instantiate the Notification
		int icon = R.drawable.icon_cash_white;
		CharSequence tickerText = "Hello";
		long when = System.currentTimeMillis();
		Notification notification = new Notification(icon, tickerText, when);
		// 3. Define the Notification's expanded message and Intent
		Context context = getApplicationContext();
		CharSequence contentTitle = "My notification";
		CharSequence contentText = "Hello World!";
		Intent notificationIntent = new Intent(this, ActRegistro.class);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);		
		
//		notification.flags   |= Notification.FLAG_AUTO_CANCEL;
	    notification.defaults |= Notification.DEFAULT_SOUND;
//	    notification.defaults |= Notification.DEFAULT_LIGHTS;
	    notification.defaults |= Notification.DEFAULT_VIBRATE;
	    
		mNotificationManager.notify(HELLO_ID, notification);
//		notification.defaults |= Notification.DEFAULT_VIBRATE;
//		long[] vibrate = {0,100,200,300};
//		notification.vibrate = vibrate;
//		
	
		
	}
}

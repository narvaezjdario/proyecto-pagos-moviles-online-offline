package com.example.pagos.gui;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.example.pagos.R;
import com.example.pagos.data.controller.PagosDBController;
import com.example.pagos.networking.HttpAsyncTask;
import com.example.pagos.networking.HttpAsyncTask.HttpInterface;
import com.example.pagos.vo.Codigos;
import com.example.pagos.vo.UsuarioVO;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class ActRegistro extends Activity implements OnClickListener,HttpInterface {
	// Base de datos
	private Cursor cursorU;
	PagosDBController data;
	// Views
	EditText eNombre, eCedula, eApellidos, eCorreo, ePass1, ePass2;
	Button btnNuevoU;

	String nombreS, apellidosS, correoS, pass1S, pass2S;
	int cedulaI;

	// server	
	UsuarioVO user;
	public static final int NUEVO_U_BD_LOCAL = 1;
	public static final int NUEVO_U_BD_SERVER = 2;
	public static final int U_EXISTENTE_EN_SERVER = 3;

	
	//L�gica para la id de la notificacion push
	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	public static final String EXTRA_MESSAGE = "message";
    private static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String PROPERTY_EXPIRATION_TIME = "onServerExpirationTimeMs";
    private static final String PROPERTY_USER = "user";

    public static final long EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;

    String SENDER_ID = "65758570462";

    static final String TAG = "GCMDemo"; 
    
    private Context context;
    private String regid;
    private GoogleCloudMessaging gcm;
    
    private EditText txtUsuario;
    private Button btnRegistrar;
	
    private ProgressDialog indicadorProgreso;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ly_act_registro);
		this.setTitle("");
		data = new PagosDBController(this);
		casting();
	}

	public void casting() {

		eNombre = (EditText) findViewById(R.id.e_nombre);
		eCedula = (EditText) findViewById(R.id.e_cedula);
		eApellidos = (EditText) findViewById(R.id.e_apellidos);
		eCorreo = (EditText) findViewById(R.id.e_correo);
		ePass1 = (EditText) findViewById(R.id.e_password);
		ePass2 = (EditText) findViewById(R.id.e_ver_contra);

		btnNuevoU = (Button) findViewById(R.id.btn_crear_usuario);
		btnNuevoU.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btn_crear_usuario:
			if (infoCorrecta())
				crearUsuario();
			break;

		default:
			break;
		}
	}

	public boolean infoCorrecta() {
		boolean rta;
		if (eCedula.getText().toString().equals(""))
			cedulaI = 0;
		else
			cedulaI = Integer.parseInt(eCedula.getText().toString());
		nombreS = eNombre.getText().toString();
		apellidosS = eApellidos.getText().toString();
		correoS = eCorreo.getText().toString();
		pass1S = ePass1.getText().toString();
		pass2S = ePass2.getText().toString();

		if (cedulaI != 0 && nombreS.length() != 0 && apellidosS.length() != 0
				&& correoS.length() != 0 && pass1S.length() != 0
				&& pass2S.length() != 0) {

			if (!pass1S.equals(pass2S)) {
				Toast.makeText(this, R.string.verificar_pass, Toast.LENGTH_LONG)
						.show();
				rta = false;
			} else
				rta = true;
		}

		else {
			Toast.makeText(this, R.string.completar_informacion,
					Toast.LENGTH_LONG).show();
			rta = false;
		}

		return rta;
	}

	public void crearUsuario() {

		if (data.searchUserByCedula(cedulaI) != null)
			Toast.makeText(this, R.string.usuario_existente, Toast.LENGTH_LONG)
					.show();
		else {

			user = new UsuarioVO();
			user.setCedula(cedulaI);
			user.setNombre(nombreS);
			user.setApellidos(apellidosS);
			user.setCorreo(correoS);
			user.setPassword(pass1S);

			if (isNetworkAvailable(ActRegistro.this)) {
				Log.i("cxRegistro", "Network available");
				indicadorProgreso = new ProgressDialog(this).show(this, getString(R.string.sincronizando_titulo), getString(R.string.sincronizando_registro));
				registroIdPush();
				
			} 
				else {
//				Log.i("cxRegistro", "Network Unavailable");
				crearUsuarioBDLocal(-1, NUEVO_U_BD_LOCAL);
			}

		}

	}

	public static boolean isNetworkAvailable(Context context) {

		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	
	
	
	public void enviarJson(){
			
		List<NameValuePair> dataPost= new ArrayList<NameValuePair>();
		
		NameValuePair n1= new BasicNameValuePair("nombre"   , nombreS);		
		NameValuePair n2= new BasicNameValuePair("cedula"   , cedulaI+"");		
		NameValuePair n3= new BasicNameValuePair("apellidos", apellidosS+"");
		NameValuePair n4= new BasicNameValuePair("correo"   , correoS+"");
		NameValuePair n5= new BasicNameValuePair("password" , pass1S+"");
		NameValuePair n6= new BasicNameValuePair("id_push"  , regid+"");		
		
		dataPost.add(n1);
		dataPost.add(n2);
		dataPost.add(n3);
		dataPost.add(n4);
		dataPost.add(n5);
		dataPost.add(n6);
		
		HttpAsyncTask task= new HttpAsyncTask(this, dataPost,Codigos.OPERACION_REGISTRO);
		
		String direccionRegistro = 
				getResources().getString(R.string.dx_base)+
				getResources().getString(R.string.dx_registro);
		task.execute(direccionRegistro);		
		
	}

	public void crearUsuarioBDLocal(long idServer, int estado) {

		// ESTADO
		// 1 CREAR LOCAL
		// 2 CREADO SERVER
		// 3 YA EXISTENTE SERVER

		ContentValues reg = new ContentValues();

		reg.put(data.CU_ID_SERVER, idServer);
		reg.put(data.CU_CEDULA, cedulaI);
		reg.put(data.CU_NOMBRE, nombreS);
		reg.put(data.CU_APELLIDOS, apellidosS);
		reg.put(data.CU_CORREO, correoS);
		reg.put(data.CU_PASSWORD, pass1S);
		reg.put(data.CU_ID_N_PUSH, regid);

		// usuario existente localmente
		if (data.searchUsuarioByCedulaPass(cedulaI,pass1S) != null) {
			data.updateUsuario(reg);
			Toast.makeText(this, R.string.usuario_actualizado, Toast.LENGTH_LONG)
					.show();
		} else {
			data.insertUsuario(reg);
			Toast.makeText(this, R.string.usuario_creado, Toast.LENGTH_LONG)
					.show();
		}

		Intent iIniciarS = new Intent(this, ActInicioSesion.class);
		startActivity(iIniciarS);

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {

		if (keyCode == event.KEYCODE_BACK) {
			vaciarET();
			Intent iSesion = new Intent(this, ActInicioSesion.class);
			startActivity(iSesion);
			return true;

		}
		return super.onKeyDown(keyCode, event);
	}

	public void vaciarET() {

		eNombre.setText("");
		eCedula.setText("");
		eApellidos.setText("");
		eCorreo.setText("");
		ePass1.setText("");
		ePass2.setText("");
	}
	
	public void registroIdPush(){
		context = getApplicationContext();
		
		//Chequemos si está instalado Google Play Services
		if(checkPlayServices())
		{
	        gcm = GoogleCloudMessaging.getInstance(ActRegistro.this);
			
	        //Obtenemos el Registration ID guardado
	        regid = getRegistrationId(context);
	
	        //Si no disponemos de Registration ID comenzamos el registro
	        if (regid.equals("")) {
	    		TareaRegistroGCM tarea = new TareaRegistroGCM();
	    		tarea.execute(nombreS);
	        }
		}
		else 
		{
            Log.i(TAG, "No se ha encontrado Google Play Services.");
        }
	}
	
	@Override
	protected void onResume() 
	{
	    super.onResume();
	    
	    checkPlayServices();
	}
	
	private boolean checkPlayServices() {
	    int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
	    if (resultCode != ConnectionResult.SUCCESS) 
	    {
	        if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) 
	        {
	            GooglePlayServicesUtil.getErrorDialog(resultCode, this,
	                    PLAY_SERVICES_RESOLUTION_REQUEST).show();
	        } 
	        else 
	        {
	            Log.i(TAG, "Dispositivo no soportado.");
	            finish();
	        }
	        return false;
	    }
	    return true;
	}
	
	private String getRegistrationId(Context context) 
	{
	    SharedPreferences prefs = getSharedPreferences(
	    		ActRegistro.class.getSimpleName(), 
	            Context.MODE_PRIVATE);
	    
	    String registrationId = prefs.getString(PROPERTY_REG_ID, "");
	    
	    if (registrationId.length() == 0) 
	    {
	        Log.d(TAG, "Registro GCM no encontrado.");
	        return "";
	    }
	    
	    String registeredUser = 
	    		prefs.getString(PROPERTY_USER, "user");
	    
	    int registeredVersion = 
	    		prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
	    
	    long expirationTime =
	            prefs.getLong(PROPERTY_EXPIRATION_TIME, -1);
	    
	    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault());
	    String expirationDate = sdf.format(new Date(expirationTime));
	    
	    Log.d(TAG, "Registro GCM encontrado (usuario=" + registeredUser + 
	    		", version=" + registeredVersion + 
	    		", expira=" + expirationDate + ")");
	    
	    int currentVersion = getAppVersion(context);
	    
	    if (registeredVersion != currentVersion) 
	    {
	        Log.d(TAG, "Nueva versión de la aplicación.");
	        return "";
	    }
	    else if (System.currentTimeMillis() > expirationTime)
	    {
	    	Log.d(TAG, "Registro GCM expirado.");
	        return "";
	    }
	    else if (!nombreS.equals(registeredUser))
	    {
	    	Log.d(TAG, "Nuevo nombre de usuario.");
	        return "";
	    }
	    
	    return registrationId;
	}
	
	private static int getAppVersion(Context context) 
	{
	    try
	    {
	        PackageInfo packageInfo = context.getPackageManager()
	                .getPackageInfo(context.getPackageName(), 0);
	        
	        return packageInfo.versionCode;
	    } 
	    catch (NameNotFoundException e) 
	    {
	        throw new RuntimeException("Error al obtener versión: " + e);
	    }
	}
	
	private class TareaRegistroGCM extends AsyncTask<String,Integer,String>
	{
		@Override
        protected String doInBackground(String... params) 
		{
            String msg = "";
            
            try 
            {
                if (gcm == null) 
                {
                    gcm = GoogleCloudMessaging.getInstance(context);
                }
                
                //Nos registramos en los servidores de GCM
                regid = gcm.register(SENDER_ID);
                
                Log.d(TAG, "Registrado en GCM: registration_id=" + regid);

                //Nos registramos en nuestro servidor
                boolean registrado = registroServidor(params[0], regid);

                //Guardamos los datos del registro
                enviarJson();
                if(registrado)
                {
                	setRegistrationId(context, params[0], regid);
                }
            } 
            catch (IOException ex) 
            {
            	Log.d(TAG, "Error registro en GCM:" + ex.getMessage());
            }
            
            return msg;
        }
	}
	
	private void setRegistrationId(Context context, String user, String regId) 
	{
	    SharedPreferences prefs = getSharedPreferences(
	    		ActRegistro.class.getSimpleName(), 
	            Context.MODE_PRIVATE);
	    
	    int appVersion = getAppVersion(context);
	    
	    SharedPreferences.Editor editor = prefs.edit();
	    editor.putString(PROPERTY_USER, user);
	    editor.putString(PROPERTY_REG_ID, regId);
	    editor.putInt(PROPERTY_APP_VERSION, appVersion);
	    editor.putLong(PROPERTY_EXPIRATION_TIME, 
	    		System.currentTimeMillis() + EXPIRATION_TIME_MS);
	    
	    editor.commit();
	}
	
	private boolean registroServidor(String usuario, String regId)
	{
	
//		toast("Usuario:"+usuario+"- id:"+regId);
		/*boolean reg = false;
		
		final String NAMESPACE = "http://sgoliver.net/";
		final String URL="http://10.0.2.2:1634/ServicioRegistroGCM.asmx";
		final String METHOD_NAME = "RegistroCliente";
		final String SOAP_ACTION = "http://sgoliver.net/RegistroCliente";

		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
		
		request.addProperty("usuario", usuario); 
		request.addProperty("regGCM", regId); 

		SoapSerializationEnvelope envelope = 
				new SoapSerializationEnvelope(SoapEnvelope.VER11);
		
		envelope.dotNet = true; 

		envelope.setOutputSoapObject(request);

		HttpTransportSE transporte = new HttpTransportSE(URL);

		try 
		{
			transporte.call(SOAP_ACTION, envelope);

			SoapPrimitive resultado_xml =(SoapPrimitive)envelope.getResponse();
			String res = resultado_xml.toString();
			
			if(res.equals("1"))
			{
				Log.d(TAG, "Registrado en mi servidor.");
				reg = true;
			}
		} 
		catch (Exception e) 
		{
			Log.d(TAG, "Error registro en mi servidor: " + e.getCause() + " || " + e.getMessage());
		} 
		*/
		return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public void processRta(String rta) {		
		
		try {
			JSONObject object = new JSONObject(rta);	
			
			long idServer=Long.parseLong(object.getString("id_server"));			
			crearUsuarioBDLocal(idServer,3);
			indicadorProgreso.cancel();
			
		} catch (JSONException e) {			
			e.printStackTrace();
			indicadorProgreso.cancel();
		}
		
	}

}

package com.example.pagos.networking;

import java.io.IOException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;

import com.example.pagos.vo.Codigos;

import android.os.AsyncTask;

public class HttpAsyncTask extends AsyncTask<String, Integer, String> {

	public static final int METHOD_POST = 0;
	public static final int METHOD_GET = 1;

	int operacion = 0;
	public static boolean login;

	public interface HttpInterface {
		public void processRta(String rta);
	}

	HttpInterface httpI;

	int httpMethod;

	String dataGet;
	List<NameValuePair> dataPost;

	// Constructor para metodo GET
	public HttpAsyncTask(HttpInterface httpI, String dataGet) {
		this.dataGet = dataGet;
		this.httpI = httpI;
		httpMethod = METHOD_GET;
	}

	// Constructor para metodo POST
	public HttpAsyncTask(HttpInterface httpI, List<NameValuePair> dataPost) {
		this.dataPost = dataPost;
		this.httpI = httpI;
		httpMethod = METHOD_POST;
	}

	public HttpAsyncTask(HttpInterface httpI, List<NameValuePair> dataPost,
			int operacion) {
		this.dataPost = dataPost;
		this.httpI = httpI;
		this.operacion = operacion;
		httpMethod = METHOD_POST;
	}

	@Override
	protected String doInBackground(String... params) {
		String rta = null;
		HttpConnection httpCx = new HttpConnection(params[0]);

		try {
			if (httpMethod == METHOD_POST)

				// OPERACION_REGISTRO=0;
				// OPERACION_LOGIN=1;
				// OPERACION_PAGAR=2;
				// OPERACION_TRANSFERIR=3;
				// OPERACION_CERRAR_SESION=4;

				switch (operacion) {
				case Codigos.OPERACION_REGISTRO:
					rta = httpCx.requestByPost(dataPost, Codigos.OPERACION_REGISTRO);
					break;

				case Codigos.OPERACION_LOGIN:
					rta = httpCx.requestByPost(dataPost, Codigos.OPERACION_LOGIN);
					break;
				case Codigos.OPERACION_PAGAR:
					rta = httpCx.requestByPost(dataPost, Codigos.OPERACION_PAGAR);
					break;

				case Codigos.OPERACION_TRANSFERIR:

					break;
					
				case Codigos.OPERACION_CERRAR_SESION:
					rta = httpCx.requestByPost(dataPost, Codigos.OPERACION_CERRAR_SESION);
					break;

				default:
					break;

				}
			else
				rta = httpCx.requestByGet(dataGet);

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return rta;
	}

	@Override
	protected void onPostExecute(String result) {
		httpI.processRta(result);
	}

}

package com.example.pagos.networking;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import com.example.pagos.R;
import com.example.pagos.gui.ActOpciones;
import com.example.pagos.vo.Codigos;


public class HttpConnection {

	// Clase HttpClient nos permite ejecutar las
	// solicitudes HTTP y obtener las respuetas
	DefaultHttpClient client;

	// Url del recurso web
	String url;

	List<Cookie> cookies;
	public static Cookie sessionInfo;


	// Constructor de la clase
	// Fijamos la url y instanciamos el HttpClient
	public HttpConnection(String url) {
		
//		 HttpParams params = new BasicHttpParams();
//		    HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//		    HttpProtocolParams.setContentCharset(params, HTTP.DEFAULT_CONTENT_CHARSET);
//		    HttpProtocolParams.setUseExpectContinue(params, true);
//		
//		 SchemeRegistry schReg = new SchemeRegistry();
//		    schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 8080));
//		    schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 8443));
//		    ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);		
//	    return new DefaultHttpClient(conMgr, params);		    
		this.url = url;
		client = new DefaultHttpClient();		

   
		
		

	}

	// Metodo para obtener un recurso web por POST
	public String requestByPost(List<NameValuePair> data, int operacion)
			throws ClientProtocolException, IOException {
		
		

		// Creamos un objeto de tipo HttpPost
		HttpPost request = new HttpPost(url);
		// Creamos el formulario de datos
		UrlEncodedFormEntity form = new UrlEncodedFormEntity(data);
		// Fijamos los datos en la solicitud POST
		request.setEntity(form);

		// Ejecutamos la solicitud con el client Http
		// y obtenemos la respuesta (HttpResponse)
		// HttpResponse response = client.execute(request);
		HttpResponse response;

		switch (operacion) {
		// obtenemos la cookie para su posterior almacenamiento
		case Codigos.OPERACION_LOGIN:
			response = client.execute(request);
			CookieStore cookieStore = ((AbstractHttpClient) client)
					.getCookieStore();

			cookies = cookieStore.getCookies();
			List<Cookie> listcookies = cookieStore.getCookies();
			for (Cookie cookie : listcookies) {
				sessionInfo = cookie;
			}
			break;

		case Codigos.OPERACION_REGISTRO:
			response = client.execute(request);
			break;

		// Debemos adicionar la cookie
		case Codigos.OPERACION_PAGAR:

			//Adicionamos la cookie para verificar sesion
			BasicClientCookie cookie;
			cookie = new BasicClientCookie("JSESSIONID",
					ActOpciones.cookieValue);
			
//			cookie.setDomain("192.168.190.103");
			cookie.setDomain("pagosmoviles-unicaucadigital.rhcloud.com");
			
			BasicCookieStore lCS = new BasicCookieStore();
			lCS.addCookie(cookie);
			client.setCookieStore(lCS);
			response = client.execute(request);

			break;
			
		case Codigos.OPERACION_CERRAR_SESION:
			
			//Adicionamos la cookie para verificar sesion
			BasicClientCookie cookieCerrarS;
			cookieCerrarS = new BasicClientCookie("JSESSIONID",
					ActOpciones.cookieValue);
//			cookieCerrarS.setDomain("192.168.190.103");
			cookieCerrarS.setDomain("pagosmoviles-unicaucadigital.rhcloud.com");
			
			BasicCookieStore lCSCerrar = new BasicCookieStore();
			lCSCerrar.addCookie(cookieCerrarS);
			client.setCookieStore(lCSCerrar);
			response = client.execute(request);
			
			break;

		default:
			response = client.execute(request);
			break;
		}

		return processResponse(response);

		// Procesamos la respuesta

	}

	// Metodo para obtener un recurso por GET
	public String requestByGet(String data) throws ClientProtocolException,
			IOException {

		// Creamos un objeto de tipo HttpGet
		// y concatenamos los datos en la url
		HttpGet request = new HttpGet(url + "?" + data);

		// Ejecutamos la solicitud
		// y obtenemos la respuesta

		HttpResponse response = client.execute(request);

		// Procesamos la respuesta
		return processResponse(response);
	}

	// Metodo para procesar el HttpResponse
	// Obtenemos el recurso y lo convertimos a String<
	public String processResponse(HttpResponse response) throws ParseException,
			IOException {

		String rta = EntityUtils.toString(response.getEntity());

		return rta;
	}
}
